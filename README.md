# final-project

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### ini adalah link aplikasinya
https://frosty-hamilton-6010b9.netlify.app/

### ini adalah link demo nya
https://drive.google.com/file/d/1kGmzC2dSvZyy3kDF3XJYezh_jpFXb_1m/view?usp=sharing

### ini screenshoot aplikasinya
https://drive.google.com/file/d/1qCQHcxtQavw0svdjW8_8Kber5djPatRg/view?usp=sharing
https://drive.google.com/file/d/1h9Ms7qSbxg0-cPX5oATVCP-08hVsL138/view?usp=sharing